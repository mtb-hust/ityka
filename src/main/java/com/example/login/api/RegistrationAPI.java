package com.example.login.api;

import com.example.login.dto.RegistrationInfoDTO;
import com.example.login.service.impl.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class RegistrationAPI {
    private final RegistrationService registrationService;

    @PostMapping(value = "/api/registration")
    public String register(@RequestBody RegistrationInfoDTO registrationInfoDTO){
        return registrationService.register(registrationInfoDTO);
    }

    @GetMapping(value = "/api/registration/confirmation")
    public String confirm(@RequestParam("token") String token){
        return registrationService.confirmToken(token);
    }
}
